/* @flow */

export type Action<P> = {
    type: string,
    payload: P,
};

export type PayloadReducer<S, P> = (state: S, payload: P) => S|() => S;

export type ActionReducer<S, P> = (state: S, action: Action<P>) => S;

export type Actor<S, P> = {
    (payload: P): Action<P>,
    type: string,
    reducer: ActionReducer<S, P>,
};

export const actor = <S, P>(type: string, payloadReducer: PayloadReducer<S, P>): Actor<S, P> => {
    const reducer = (state: S, action: Action<P>): S => {
        if (action.type !== type) {
            return state;
        }

        const result = payloadReducer(state, action.payload);
        return typeof result === 'function' ? result() : result; 
    }

    const actionCreator = (payload: P): Action<P> => ({ type, payload });
    actionCreator.type = type;
    actionCreator.reducer = reducer;

    return actionCreator;
}

export const combineReducers = <S>(...actorMaps: {[key: string]: Actor<S, any>}[]): ActionReducer<S, Action<any>> => {
    const reducerMap: {[type: string]: ActionReducer<S, Action<any>>} = {};

    actorMaps.forEach(actorMap => {
        Object.keys(actorMap).forEach(key => {
            const actor = actorMap[key];
            reducerMap[actor.type] = actor.reducer;
        });
    });

    return (state: S, action: Action<any>) => reducerMap[action.type] ?
        reducerMap[action.type](state, action) :
        state;
};

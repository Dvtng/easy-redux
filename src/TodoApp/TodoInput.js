/* @flow */

import React from 'react';
import {connect} from 'react-redux';
import type {State} from './state';
import {updateInputText, submitInputText} from './actors';

type ValueProps = {
    text: string,
};

type CallbackProps = {
    updateInputText: (text: string) => void,
    submitInputText: () => void,
};

type Props = ValueProps & CallbackProps;

const onKey = (keyCode: number, handler: (Event) => void) => (e: Event): void => {
    if (e.keyCode === keyCode) {
        handler(e);
    }
}

const TodoInput = (props: Props) => (
    <input
        value={props.text}
        onChange={e => props.updateInputText(e.target.value)}
        onKeyDown={onKey(13, props.submitInputText)}
    />
);

export default connect((state: State): ValueProps => ({
    text: state.inputText,
}), (dispatch): CallbackProps => ({
    updateInputText: (text: string) => dispatch(updateInputText({text})),
    submitInputText: () => dispatch(submitInputText()),
}))(TodoInput);
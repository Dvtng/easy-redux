/* @flow */

import React from 'react';
import {connect} from 'react-redux';
import type {State} from './state';
import TodoItem from './TodoItem';

type Props = {
    todoIds: number[],
};

const TodoItemList = (props: Props) => (
    <div>
        {props.todoIds.map(todoId => <TodoItem key={todoId} todoId={todoId}/>)}
    </div>
);

export default connect((state: State): Props => ({
    todoIds: Object.keys(state.todos).map(Number),
}))(TodoItemList);
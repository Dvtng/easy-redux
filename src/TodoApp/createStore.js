/* @flow */

import {createStore} from 'redux';
import * as actors from './actors';
import {initialState} from './state';
import {combineReducers} from '..';

export default () => createStore(
    combineReducers(actors),
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
/* @flow */

import React from 'react';
import {connect} from 'react-redux';
import type {State} from './state';

type Props = {
    title: string,
};

const TodoItem = (props: Props) => (
    <div>{props.title}</div>
);

export default connect((state: State, props: {todoId: number}): Props => ({
    title: state.todos[props.todoId].title,
}))(TodoItem);
/* @flow */

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import createStore from './createStore';
import TodoInput from './TodoInput';
import TodoItemList from './TodoItemList';

const root = document.createElement('div');
if (document.body) {
    document.body.appendChild(root);
}

render(
    <Provider store={createStore()}>
        <div>
            <TodoInput/>
            <TodoItemList/>
        </div>
    </Provider>,
    root,
);
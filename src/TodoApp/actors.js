/* @flow */

import type {Actor} from '..';
import {actor} from '..';
import type {State} from './state';

export const updateInputText: Actor<State, {text: string}> = actor(
    'Update input text',
    (state, payload) => ({
        ...state,
        inputText: payload.text,
    }),
);

export const submitInputText: Actor<State, void> = actor(
    'Submit input text',
    (state) => ({
        nextTodoId: state.nextTodoId + 1,
        todos: {
            ...state.todos,
            [state.nextTodoId]: {
                title: state.inputText,
            }
        },
        inputText: '',
    }),
);
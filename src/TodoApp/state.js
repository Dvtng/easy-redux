/* @flow */

export type State = {
    nextTodoId: number,
    todos: {
        [id: number]: {
            title: string,
        },
    },
    inputText: string,
};

export const initialState: State = {
    nextTodoId: 1,
    todos: {},
    inputText: '',
};
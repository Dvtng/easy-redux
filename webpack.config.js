const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/TodoApp/index',
    output: {
        path: './build',
        filename: 'bundle.js',
    },
    devServer: {
        contentBase: './build',
    },
    devtool: '#source-map',
    plugins: [
        new HtmlWebpackPlugin(),
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                },
                exclude: /node_modules/,
            },
        ],
    },
};